/* eslint-disable no-unused-expressions */

const chai = require('chai');
//const sinon = require('sinon');
const index = require('../index');

const { expect } = chai;

describe('coding challenge spec', () => {

  it('should return an array of english items when requested', async () => {
      const result = await index.handler('en');
      expect(result).to.be.an('array');
      expect(result).to.not.be.empty;
      result.forEach(item => expect(item.lang).to.equal('en'));
  });

  it('should return an array of spanish items when requested', async () => {
      const result = await index.handler('es');
      expect(result).to.be.an('array');
      expect(result).to.not.be.empty;
      result.forEach(item => expect(item.lang).to.equal('es'));
  });

  it('should return an array of all items when language is not specified', async () => {
      const result = await index.handler();
      let languages = new Set();
      result.forEach(item => languages.add(item.lang));
      expect(result).to.be.an('array');
      expect(result).to.not.be.empty;
      expect(languages).to.include('en');
      expect(languages).to.include('es');
  });
});


