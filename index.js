const request = require("node-fetch");

const url = 'https://www.healthcare.gov/api/glossary.json';

// return an array of healthcare.gov glossary items for the language specified
// if no language is specified, return an array of all glossary items.
exports.handler = async function(language) {

		var outp = [];
	    const response = await request(url);
    	const json = await response.json();
		var count = Object.keys(json.glossary).length;

		for (var i = 0; i < count; i++) {
		if (typeof language == "undefined" || json.glossary[i].lang == language) {
			outp[i] = json.glossary[i];
		 	}
	     }
	     
		return outp;
}

